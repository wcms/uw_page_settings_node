/**
 * @file
 * Use jQuery to provide summary information inside vertical tabs.
 */

(function ($) {

  /**
   * Provide summary information for vertical tabs.
   */
  Drupal.behaviors.uw_page_settings_node_edit = {
    attach: function (context) {

      // Provide summary when editing a node.
      $('fieldset#edit-uw-page-settings-node-edit', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-uw-page-settings-node-enable:checked', context).is(':checked')) {
          vals.push(Drupal.t('Wide page'));
        }
        else {
          vals.push(Drupal.t('Standard page'));
        }
        return vals.join('<br/>');
      });

      // Provide summary during content type configuration.
      $('fieldset#edit-uw-page-settings-node', context).drupalSetSummary(function (context) {
        var vals = [];
        if ($('#edit-uw-page-settings-node-node-type', context).is(':checked')) {
          vals.push(Drupal.t('Wide page'));
        }
        else {
          vals.push(Drupal.t('Standard page'));
        }
        return vals.join('<br/>');
      });

    }
  };

})(jQuery);
