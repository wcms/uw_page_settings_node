/**
 * @file
 */

(function ($) {
  $(function () {
    // Change the width of the body ckeditor when wide page is checked.
    $widecheck = $('#edit-uw-page-settings-node-enable');
    $textformatter = $('[id ^=edit-body][id $=0-format--2]');
    // Image gallery bottom body.
    $textformatter2 = $('[id ^=edit-field-image-gallery-bottom][id $=0-format--2]');
    // Project body.
    $textformatter3 = $('[id ^=edit-field-project-description][id $=0-format--2]');
    $widecheck.click(function () {
      set_wide_text_format(this.checked);
    })
    function set_wide_text_format($checked) {
      if ($checked) {
        $textformatter.val('uw_tf_standard_wide').change();
        $textformatter2.val('uw_tf_standard_wide').change();
        $textformatter3.val('uw_tf_standard_wide').change();
        $('#field-related-link-add-more-wrapper').hide();
        $('#field-sidebar-content-add-more-wrapper').hide();
      }
      else {
        $textformatter.val('uw_tf_standard').change();
        $textformatter2.val('uw_tf_standard').change();
        $textformatter3.val('uw_tf_standard').change();
        $('#field-related-link-add-more-wrapper').show();
        $('#field-sidebar-content-add-more-wrapper').show();
      }
    }
  });
}(jQuery));
