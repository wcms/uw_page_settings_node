<?php

/**
 * @file
 * Install, update and uninstall functions for the uw_page_settings_node module.
 */

/**
 * Implements hook_schema().
 *
 * @ingroup uw_page_settings_node
 */
function uw_page_settings_node_schema() {
  $schema['uw_page_settings_node'] = array(
    'description' => 'Stores information about node authorization.',
    'fields' => array(
      'nid'    => array(
        'description' => 'Node ID that the settings are applied to.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'vid'    => array(
        'description' => 'Revision ID, as we are tracking with node revisions',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'enable' => array(
        'description' => 'Whether or not authenticated access is enabled on this node.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ),
    ),
    'primary key' => array('vid'),
    'indexes' => array(
      'nid'   => array('nid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_install().
 *
 * Convert the data of field_wide into uw_page_settings_node.
 * Remove field_wide from web page content type nodes.
 */
function uw_page_settings_node_install() {
  // Convert the data of field_wide into uw_page_settings_node.
  if (db_table_exists('field_data_field_wide') && db_table_exists('uw_page_settings_node')) {
    $result = db_query("SELECT entity_id, revision_id, field_wide_value FROM {field_data_field_wide}");
    foreach ($result as $record) {
      $data = array(
        'nid' => $record->entity_id,
        'vid' => $record->revision_id,
        'enable' => $record->field_wide_value,
      );
      drupal_write_record('uw_page_settings_node', $data);
    }
  }

  // Remove field_wide from web page content type nodes.
  $field_wide = field_info_instance('node', 'field_wide', 'uw_web_page');
  if (!empty($field_wide)) {
    field_delete_field('field_wide');
  }
}

/**
 * Implements hook_uninstall().
 *
 * We need to clean up our variables data when uninstalling our module.
 *
 * hook_uninstall() will only be called when uninstalling a module, not when
 * disabling a module. This allows our data to stay in the database if the user
 * only disables our module without uninstalling it.
 *
 * @ingroup uw_page_settings_node
 */
function uw_page_settings_node_uninstall() {
  // Simple DB query to get the names of our variables.
  $results = db_select('variable', 'v')
    ->fields('v', array('name'))
    ->condition('name', 'uw_page_settings_node_node_type_%', 'LIKE')
    ->execute();
  // Loop through and delete each of our variables.
  foreach ($results as $result) {
    variable_del($result->name);
  }
}

/**
 * Implements hook_update_N().
 */

/**
 * Fixing wide page version ids.
 *
 * ISTWCMS-2565: Removing all vids that are not in the node_revision table.
 */
function uw_page_settings_node_update_7101() {

  // ISTWCMS-2565: Tried to do this with one sql and database abstraction.
  // Could not do with database abstraction because we can not chain with joins.
  // Could not do in one sql statement because you can not delete/update
  // the same table in the sub-query as the query.
  // Query to get records that are not suppose to be
  // in the uw_page_settings_node table.
  $query = db_select('uw_page_settings_node', 'psn')
    ->fields('psn', array('nid'));

  $query->leftjoin('node_revision', 'nr', 'psn.nid = nr.nid AND psn.vid = nr.vid');

  $query->condition('nr.nid', NULL);

  $result = $query->execute();

  // Step through each result and store is record.
  while ($record = $result->fetchAssoc()) {
    $records[] = $record['nid'];
  }

  // If we have records to delete, delete them.
  if (count($records) > 0) {

    // Delete query to remove record from uw_page_settings_node table.
    $num_deleted = db_delete('uw_page_settings_node')
      ->condition('nid', $records, 'IN')
      ->execute();
  }
}
